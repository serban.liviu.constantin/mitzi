/**
 * Created by Liviu on 6/16/2015.
 */

var modal_init = function() {

    var modalWrapper = document.getElementById("modal_wrapper_about");
    var modalWindow  = document.getElementById("modal_window_about");

    var openModal = function(e)
    {
        modalWrapper.className = "overlay";
        modalWindow.style.marginTop = (-modalWindow.offsetHeight)/2 + "px";
        modalWindow.style.marginLeft = (-modalWindow.offsetWidth)/2 + "px";
        e.preventDefault ? e.preventDefault() : e.returnValue = false;
    };

    var closeModal = function(e)
    {
        modalWrapper.className = "";
        e.preventDefault ? e.preventDefault() : e.returnValue = false;
    };

    var clickHandler = function(e) {
        if(!e.target) e.target = e.srcElement;
        if(e.target.tagName == "DIV") {
            if(e.target.id != "modal_window_about") closeModal(e);
        }
    };

    var keyHandler = function(e) {
        if(e.keyCode == 27) closeModal(e);
    };

    if(document.addEventListener) {
        document.getElementById("modal_open_about").addEventListener("click", openModal, false);
        document.getElementById("modal_close_about").addEventListener("click", closeModal, false);
        document.addEventListener("click", clickHandler, false);
        document.addEventListener("keydown", keyHandler, false);
    } else {
        document.getElementById("modal_open_about").attachEvent("onclick", openModal);
        document.getElementById("modal_close_about").attachEvent("onclick", closeModal);
        document.attachEvent("onclick", clickHandler);
        document.attachEvent("onkeydown", keyHandler);
    }
};

if(document.addEventListener) {
    document.addEventListener("DOMContentLoaded", modal_init, false);
} else {
    window.attachEvent("onload", modal_init);
}
