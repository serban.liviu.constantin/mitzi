package com.inspire.database;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DB {

    private static final Logger LOGGER = Logger.getLogger(DB.class);

    private static final String JDBC_PG_DRIVER = "jdbc:sqlite:";

    private static final String SQL_GET_QUEUE_NAME = "SELECT q_url FROM queue_name WHERE q_name = ";
    private static final String SQL_GET_URLS= "SELECT c_url FROM crawl_urls WHERE c_type = ";

    private static final String SQL_INSERT_FEEDBACK_RECORD = "INSERT INTO feedback(name,email,subject,message,date) " +
            "VALUES(?,?,?,?,?)";

    public static Connection getDataBaseConnection(String host, String dataSource, String user, String password) {

        Connection conn = null;

        try {

            Class.forName("org.sqlite.JDBC");

            String connectUrl = JDBC_PG_DRIVER + dataSource;
            conn = DriverManager.getConnection(connectUrl, user, password);

            return conn;

        }catch (SQLException sqle) {
            LOGGER.fatal("SQL Exception caught ", sqle);
        } catch (ClassNotFoundException cnfe) {
            LOGGER.fatal("Class no Found ", cnfe);
        }

        return null;
    }

    public static void closeConnection(Connection connection) {

        try {

            if (connection != null) {
                connection.close();
            }

        } catch (SQLException sqle) {
            LOGGER.fatal("SQL Exception caught ", sqle);
        }
    }

    public static void sqlInsertFeedbackRecord(Connection connection,
                                                 String name, String email, String subject, String message, String date) {

        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SQL_INSERT_FEEDBACK_RECORD);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, subject);
            preparedStatement.setString(4, message);
            preparedStatement.setString(5, date);

            // execute insert SQL stetement
            preparedStatement.executeUpdate();

            System.out.println("Record is inserted into DBUSER table!");

        } catch (SQLException sqle) {
            LOGGER.fatal("SQL Exception caught ", sqle);
        } finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
            } catch (SQLException sql) {
                LOGGER.fatal("SQL Exception caught in finally clause", sql);
            }
        }
    }
}
