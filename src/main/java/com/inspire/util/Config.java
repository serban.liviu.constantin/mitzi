package com.inspire.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Config {

    @Value("${indexDir}")
    private String indexDir;

    @Value("${spellDir}")
    private String spellDir;

    @Value("${autocompleteDir}")
    private String autocompleteDir;

    @Value("${ldaDir}")
    private String lda;

    @Value("${feedbackDB}")
    private String db;

    @Value("${stopwordsFile}")
    private String stopwords;

    private String appPath = Config.class.getProtectionDomain().getCodeSource().getLocation().getPath();

    public Config() {
    }

    public String getIndexDir() {
        return indexDir;
    }

    public String getSpellDir() {
        return spellDir;
    }

    public String getAutocompleteDir() {
        return autocompleteDir;
    }

    public String getLda() {
        return lda;
    }

    public String getDb() {
        return db;
    }

    public String getStopwords() {
        return stopwords;
    }

    public String getAppPath() {
        return appPath;
    }
}
