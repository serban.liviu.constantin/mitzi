package com.inspire.session;

import com.inspire.servlet.IndexServlet;
import com.inspire.util.Config;
import com.inspire.util.Constants;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Date;

@Component
public class SearchSession {

    @Autowired
    private Config config;

    private SearchSession() {}

     public HttpSession getOrSetSearchSession(HttpServletRequest request)
            throws IOException {

        HttpSession session = request.getSession(true);
        Date createTime = new Date(session.getCreationTime());
        Date lastAccessTime = new Date(session.getLastAccessedTime());

        String userIDKey = "userID";
        String userID = "ABCD";

        // Check if this is new comer on your web page.
        if (session.isNew()){

            String path = config.getAppPath() + config.getIndexDir();
            DirectoryReader ireader = DirectoryReader.open(FSDirectory.open(Paths.get(path)));

            IndexSearcher indexSearcher = new IndexSearcher(ireader);

            session.setAttribute(userIDKey, userID);

            session.setAttribute(Constants.SESSION_LAST_INDEX, -1);

            session.setAttribute(Constants.SESSION_INDEX_SEARCHER, indexSearcher);
        }

        System.out.println("SessionId: " + session.getId());
        System.out.println("Create Time : " + createTime);
        System.out.println("Last Access Time : " + lastAccessTime);
        System.out.println("Last Search Query : " + session.getAttribute("lastSearchQuery"));

        return session;
    }
}
