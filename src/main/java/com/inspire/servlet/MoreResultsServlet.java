package com.inspire.servlet;

import com.inspire.search.Search;
import com.inspire.session.SearchSession;
import com.inspire.stem.Stemmer;
import com.inspire.util.Constants;
import org.apache.log4j.Logger;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/more")
public class MoreResultsServlet extends HttpServlet {

    @Autowired
    private SearchSession searchSession;

    private Search searchInterface;
    private static final Logger LOGGER = Logger.getLogger(MoreResultsServlet.class);

    public void init() throws ServletException {
        searchInterface = Search.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = searchSession.getOrSetSearchSession(request);

        int lastIndex = (int) session.getAttribute(Constants.SESSION_LAST_INDEX);
        List<ScoreDoc> list = (List<ScoreDoc>)session.getAttribute(Constants.SESSION_LIST_DOC_IDS);
        IndexSearcher indexSearcher = (IndexSearcher)session.getAttribute(Constants.SESSION_INDEX_SEARCHER);

        JSONObject jsonObject;

        try {
            int listSize = list.size();
            if (listSize - lastIndex <= 10) {
                session.setAttribute(Constants.SESSION_LAST_INDEX, listSize);
                jsonObject = searchInterface.getMoreResults(list, indexSearcher, lastIndex, listSize);
                jsonObject.put(Constants.JSON_FINAL, "1");
            } else {
                session.setAttribute(Constants.SESSION_LAST_INDEX, lastIndex + 10);
                jsonObject = searchInterface.getMoreResults(list, indexSearcher, lastIndex, lastIndex + 10);
                jsonObject.put(Constants.JSON_FINAL, "0");
            }

            jsonObject.put(Constants.JSON_RESULTS, "1");
            jsonObject.put(Constants.JSON_TOTAL, list.size());

            JSONObject json = new JSONObject();
            json.put(Constants.JSON_DOCUMENTS, jsonObject);

            response.getWriter().write(json.toString());

        } catch (JSONException jse) {
            LOGGER.fatal("json exception caught ", jse);
        }
    }
}
