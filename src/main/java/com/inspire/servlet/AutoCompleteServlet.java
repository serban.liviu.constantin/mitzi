package com.inspire.servlet;

import com.inspire.util.Constants;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


@WebServlet("/autocomplete")
public class AutoCompleteServlet extends HttpServlet {

    private static final String F_TITLE = "title";

    private static final String F_TITLE_TEMP = "titleTemp";

    private static final String F_FREQ = "frequency";

    private static final int MAX_PREFIX_LENGTH = 10;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String query = request.getParameter("query");
        if (query == null || query.length() == 0)
            return;

        response.setContentType("application/json");
        String[] array = suggestSimilar(query, 5);

        try {
            HashSet<String> set = new HashSet<>();
            JSONArray jsonArray = new JSONArray();
            for (String s : array) {
                if(!set.contains(s)) {
                    set.add(s);
                    jsonArray.put(s);
                }
            }

            JSONObject j = new JSONObject();
            j.put("suggestions", jsonArray);
            //String json = StringEscapeUtils.unescapeJson(j.toString());

            response.getWriter().write(j.toString());
        } catch (JSONException jse) {

        }
    }


    public String[] suggestSimilar(String word, int numSug) throws IOException {
        // obtainSearcher calls ensureOpen
        DirectoryReader ireader = DirectoryReader.open(FSDirectory.open(Paths.get(com.inspire.servlet.IndexServlet.cfgPaths.getAutocompleteDir())));

        IndexSearcher indexSearcher = new IndexSearcher(ireader);

        try{
            BooleanQuery query = new BooleanQuery();
            List<String[]> grams = formGrams(word);
            String key;
            for (String[] gramArray : grams) {
                for (int i = 0; i < gramArray.length; i++) {
                    key = "start" + gramArray[i].length(); // form key
                    add(query, key, gramArray[i]);
                }
            }


            int maxHits = 2 * numSug;

            //First sort on similarity, then on popularity (based on frequency in the source index)
            SortField[] sortFields = {SortField.FIELD_SCORE, new SortField(F_FREQ, SortField.Type.LONG, true)};
            Sort sort = new Sort(new SortField(F_FREQ, SortField.Type.INT));

            TopDocs topDocs = indexSearcher.search(query, maxHits);

            ScoreDoc[] hits = topDocs.scoreDocs;
            //indexSearcher.search(query, null, maxHits).scoreDocs;
            int stop = Math.min(hits.length, maxHits);
            String[] toReturn = new String[stop];

            for (int i = 0; i < stop; i++) {
                toReturn[i] =  indexSearcher.doc(hits[i].doc).get(F_TITLE); // get orig word
            }
            return toReturn;
        } finally {

        }
    }

    private static void add(BooleanQuery bq, String key, String value) {
        TermQuery tq = new TermQuery(new Term(key, value));
        bq.add(tq, BooleanClause.Occur.MUST);
    }


    private static List<String[]> formGrams(String text) {
        //first split into tokens to match words in phrases
        text = text.toLowerCase(); //search prefixes in lower case!
        int len, tokenlen;
        ArrayList<String[]> grams = new ArrayList<String[]>();

        len = 3;
        tokenlen = Math.min(text.length(), MAX_PREFIX_LENGTH);
        if (tokenlen < 3) {
            len = tokenlen;
        }

        String[] res = new String[len];
        for (int i = 0; i < len; i++) {
            res[i] = text.substring(0, tokenlen-i);
        }
        grams.add(res);

        return grams;
    }

    private static int getMax(int l) {
        if (l > MAX_PREFIX_LENGTH) {
            return MAX_PREFIX_LENGTH;
        }
        return l;
    }
}
