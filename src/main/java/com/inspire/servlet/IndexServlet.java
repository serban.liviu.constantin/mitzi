package com.inspire.servlet;

import com.inspire.util.Config;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/")
public class IndexServlet extends HttpServlet {

    public static Config cfgPaths;

    public void init(ServletConfig config) throws ServletException {

        /*
        String index = config.getInitParameter("indexDir");
        String autocomplete = config.getInitParameter("autocompleteDir");
        String spell = config.getInitParameter("spellDir");
        String lda = config.getInitParameter("ldaDir");
        String db = config.getInitParameter("feedbackDB");
        String stopwords = config.getInitParameter("stopwordsFile");

        index = config.getServletContext().getRealPath(index);
        autocomplete = config.getServletContext().getRealPath(autocomplete);
        spell = config.getServletContext().getRealPath(spell);
        lda = config.getServletContext().getRealPath(lda);
        db = config.getServletContext().getRealPath(db);
        stopwords = config.getServletContext().getRealPath(stopwords);

        cfgPaths = new CfgPaths(index, spell, autocomplete, lda, db, stopwords);
        */

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("/src/main/webapp/hello.ftl").forward(request, response);
    }
}
