package com.inspire.servlet;

import com.inspire.database.DB;
import com.inspire.util.Constants;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

@WebServlet("/feedback")
public class FeedbackServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(FeedbackServlet.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Connection conn = null;

        do {
            String name = request.getParameter("name");
            String email = request.getParameter("email");
            String subject = request.getParameter("subject");
            String message = request.getParameter("message");

            conn = DB.getDataBaseConnection("", IndexServlet.cfgPaths.getDb(), "", "");
            if (conn == null) {
                LOGGER.error("Failed to connect to  the Database");
                break;
            }

            DB.sqlInsertFeedbackRecord(conn, name, email, subject, message, new Date().toString());

        } while (false);

        if (conn != null)
            DB.closeConnection(conn);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
