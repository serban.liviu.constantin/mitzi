package com.inspire.didyoumean;

import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.Directory;

import java.io.IOException;

public class DidYouMeanParserImpl implements DidYouMeanParser{
    private String defaultField;
    private Directory spellCheckerDirectory;

    public DidYouMeanParserImpl(String defaultField, Directory spellCheckerDirectory) {
        this.defaultField = defaultField;
        this.spellCheckerDirectory = spellCheckerDirectory;
    }

    public Query parse(String queryString) throws ParseException {
        return new TermQuery(new Term(defaultField, queryString));
    }

    public Query suggest(String queryString) throws ParseException {
        try {
            SpellChecker spellChecker = new SpellChecker(spellCheckerDirectory);
            if (spellChecker.exist(queryString))
                return null;
            String [] similarWords = spellChecker.suggestSimilar(queryString, 1);
            if (similarWords.length == 0)
                return null;

            return new TermQuery(new Term(defaultField, similarWords[0]));
        } catch (IOException ioe) {
            throw  new ParseException(ioe.getMessage());
        }
    }
}
