package com.inspire.stem;


import com.inspire.util.Config;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.tartarus.snowball.ext.PorterStemmer;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class Stemmer {

   @Autowired
   private Config config;

    private PorterStemmer stemmer = new PorterStemmer();

    private static List<String> stopWords = new ArrayList<>();
    private static final Logger LOGGER = Logger.getLogger(Stemmer.class);

    public void buildEnglishStopWordList() {

        try {

            String path = Stemmer.class.getProtectionDomain().getCodeSource().getLocation().getPath();

            path = config.getAppPath() + config.getStopwords();
            FileInputStream fstream = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

            try {

                String line = br.readLine();

                while (line != null) {
                    stopWords.add(line);
                    line = br.readLine();
                }

            } finally {
                try {
                    fstream.close();
                    br.close();
                } catch (IOException ioe) {
                    LOGGER.fatal("Exception caught while closing the stream object in [finally] clause ", ioe);
                }
            }

        } catch (Exception e) {
            LOGGER.error("Exception caught in buildEnglishStopWordList() method ", e);
        }
    }

    public String stem(String content) {
        String result = "";
        do {
            if (content == null || content.isEmpty())
                break;

            content = deleteSpecialChars(content);
            List<String> list = scanAndDeleteStopWords(content);
            for (String term : list) {
                result += stemTerm(term) + " ";
            }
        } while (false);
        return result;
    }

    private String stemTerm (String term) {
        stemmer.setCurrent(term);
        stemmer.stem();
        return stemmer.getCurrent();
    }

    private List<String> scanAndDeleteStopWords(String paragraph) {
        List<String> finalWordsForIndexing = new ArrayList<String>();
        String tagText = deleteSpecialChars(paragraph);

        List<String> list = Arrays.asList(tagText.toLowerCase().split("\\s+"));
        for (String strElem : list) {
            if (!stopWords.contains(strElem)) {
                finalWordsForIndexing.add(strElem);
            }
        }
        return finalWordsForIndexing;
    }

    private String deleteSpecialChars(String str) {
        str = str.toLowerCase();
        str = str.replaceAll("[\\(\\)\\[\\]\\{\\}\\+=\\*\\^\\\"\\.\\-\\?\\\\!|~_@#$%&'<>,:;'`]", " ");
        str = str.replaceAll("\\s+", " ");
        return str;
    }
}
