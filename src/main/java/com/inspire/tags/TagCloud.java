package com.inspire.tags;


import com.inspire.util.Fields;
import org.apache.lucene.document.Document;

import java.util.*;

public class TagCloud {

    private TagCloud() {}

    public static final TagCloud INSTANCE = new TagCloud();

    public static TagCloud getInstance() {
        return INSTANCE;
    }

    public void addToNerSet(Document document, Map<String, Map<String, Integer>> map) {
        String persons = document.get(Fields.FIELD_PERSONS).replaceAll("[\\]\\[]", "");
        String locations = document.get(Fields.FIELD_LOCATIONS).replaceAll("[\\]\\[]", "");
        String organizations  = document.get(Fields.FIELD_ORGANIZATIONS).replaceAll("[\\]\\[]", "");;
        String misc = document.get(Fields.FIELD_MISC).replaceAll("[\\]\\[]", "");;

        List<String> per = Arrays.asList(persons.split(","));

        List<String> loc = Arrays.asList(locations.split(","));

        List<String> org = Arrays.asList(organizations.split(","));

        List<String> other = Arrays.asList(misc.split(","));

        for (String str: per) {
            Map<String, Integer> map1 = new HashMap<>();
            map1 = map.get("per");

            if (str.isEmpty())
                continue;

            str = str.replaceAll("^\\s+","");
            str = str.replaceAll("\\s+$","");

            List<String> l = Arrays.asList(str.split("\\s+"));
            if (l.size() == 1)
                continue;

            Integer count = map1.get(str);
            if (count == null) {
                map1.put(str, 1);
            } else {
                map1.remove(str);
                map1.put(str, count + 1);
            }
        }

        for (String str: loc) {
            Map<String, Integer> map1 = new HashMap<>();
            map1 = map.get("loc");

            if (str.isEmpty())
                continue;

            str = str.trim();
            Integer count = map1.get(str);
            if (count == null) {
                map1.put(str, 1);
            } else {
                map1.remove(str);
                map1.put(str, count + 1);
            }
        }

        for (String str: org) {
            Map<String, Integer> map1 = new HashMap<>();
            map1 = map.get("org");

            if (str.isEmpty())
                continue;

            str = str.trim();
            Integer count = map1.get(str);
            if (count == null) {
                map1.put(str, 1);
            } else {
                map1.remove(str);
                map1.put(str, count + 1);
            }
        }

        for (String str: other) {
            Map<String, Integer> map1 = new HashMap<>();
            map1 = map.get("misc");

            if (str.isEmpty())
                continue;

            str = str.trim();
            Integer count = map1.get(str);
            if (count == null) {
                map1.put(str, 1);
            } else {
                map1.remove(str);
                map1.put(str, count + 1);
            }
        }
    }

    public List<Tag> getNerList(Map<String, Integer> map) {
        ValueComparator bvc =  new ValueComparator(map);
        TreeMap<String,Integer> sorted_map = new TreeMap<String,Integer>(bvc);

        sorted_map.putAll(map);

        int count = 0;
        List<Tag> list  = new ArrayList<>();
        for (String s : sorted_map.keySet()) {
            list.add(new Tag(s, map.get(s)));
            count++;
            if (count == 5)
                break;
        }
        return list;
    }

    class ValueComparator implements Comparator<String> {

        Map<String, Integer> base;
        public ValueComparator(Map<String, Integer> base) {
            this.base = base;
        }

        // Note: this comparator imposes orderings that are inconsistent with equals.
        public int compare(String a, String b) {
            if (base.get(a) >= base.get(b)) {
                return -1;
            } else {
                return 1;
            } // returning 0 would merge keys
        }
    }
}
