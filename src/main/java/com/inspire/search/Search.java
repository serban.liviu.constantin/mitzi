package com.inspire.search;


import com.inspire.didyoumean.DidYouMeanParserImpl;
import com.inspire.servlet.IndexServlet;
import com.inspire.servlet.SimilarityServlet;
import com.inspire.stem.Stemmer;
import com.inspire.tags.Tag;
import com.inspire.tags.TagCloud;
import com.inspire.util.Constants;
import com.inspire.util.Fields;
import com.inspire.util.Series;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.store.FSDirectory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class Search {


    @Autowired
    private Stemmer stemmer;

    private Search() {}

    private static final Search INSTANCE = new Search();

    public static Search getInstance() {
        return INSTANCE;
    }

    private static final Logger LOGGER = Logger.getLogger(Search.class);

    public TopDocs searchAll(String searchString, IndexSearcher indexSearcher) {

        TopDocs topDocs = null;

        do {
            try {

                searchString = stemmer.stem(searchString);
                String words[] = searchString.split("\\s+");
                BooleanQuery bq = new BooleanQuery();
                for (int i = 0; i < words.length; i++) {
                    if (words[i].isEmpty())
                        continue;
                    TermQuery tq = new TermQuery(new Term(Fields.FIELD_INDEX, words[i]));
                    bq.add(tq, BooleanClause.Occur.MUST);
                }

                indexSearcher.setSimilarity(new BM25Similarity((float) 1.2, (float) 0.75));

                topDocs = indexSearcher.search(bq, 1);
                if (topDocs.totalHits == 0)
                    break;

                topDocs = indexSearcher.search(bq, topDocs.totalHits);

            } catch (IOException ioe) {
                LOGGER.fatal("io excetion caught while in method search() ", ioe);
            }
        } while (false);
        return topDocs;
    }

    public TopDocs search(String searchString, IndexSearcher indexSearcher, int resultsNo, boolean searchAll) {

        TopDocs topDocs = null;

        do {
            try {

                if (resultsNo <= 0)
                    break;

                searchString = stemmer.stem(searchString);
                String words[] = searchString.split("\\s+");
                BooleanQuery bq = new BooleanQuery();
                for (int i = 0; i < words.length; i++) {
                    if (words[i].isEmpty())
                        continue;
                    TermQuery tq = new TermQuery(new Term(Fields.FIELD_INDEX, words[i]));
                    bq.add(tq, BooleanClause.Occur.MUST);
                }

                indexSearcher.setSimilarity(new BM25Similarity((float) 1.2, (float) 0.75));

                topDocs = indexSearcher.search(bq, resultsNo);
                if (topDocs.totalHits == 0)
                    break;

                if (searchAll)
                    topDocs = indexSearcher.search(bq, topDocs.totalHits);

            } catch (IOException ioe) {
                LOGGER.fatal("io excetion caught while in method search() ", ioe);
            }
        } while (false);
        return topDocs;
    }


    public JSONObject getSearchResults(ScoreDoc[] hits, IndexSearcher indexSearcher, int low, int high)
            throws JSONException {

        JSONObject jsonObject = null;
        JSONArray array = new JSONArray();

        do {
            if ((low > high) || (low < 0 || high < 0))
                break;

            Map<String, Map<String, Integer>> nerSet = getSearchResultsInternal(hits, array, indexSearcher, low, high);
            if (nerSet == null)
                break;

            jsonObject = new JSONObject();
            jsonObject.put(Constants.JSON_TAGS, new JSONArray());
            jsonObject.put(Constants.JSON_DOCS, array);

        } while (false);
        return jsonObject;
    }

    public JSONObject getSearchResults(List<ScoreDoc> list, IndexSearcher indexSearcher, int low, int high)
            throws JSONException, IOException {

        JSONObject jsonObject = null;
        JSONArray array = new JSONArray();

        do {
            if ((low > high) || (low < 0 || high < 0))
                break;

            array = getSearchResultsFromScoreDocs(list, indexSearcher, low, high);
            if (array == null)
                break;

            JSONArray tags = getRelatedSearches(list, indexSearcher);
            if (tags == null)
                break;

            jsonObject = new JSONObject();
            jsonObject.put(Constants.JSON_TAGS, tags);
            jsonObject.put(Constants.JSON_DOCS, array);

        } while (false);
        return jsonObject;
    }

    public JSONObject getMoreResults(List<ScoreDoc> list, IndexSearcher indexSearcher, int low, int high)
            throws JSONException {

        JSONObject jsonObject = null;
        JSONArray array = new JSONArray();

        do {
            if ((low > high) || (low < 0 || high < 0))
                break;

            array = getSearchResultsFromScoreDocs(list, indexSearcher, low, high);
            if (array == null)
                break;

            jsonObject = new JSONObject();
            jsonObject.put(Constants.JSON_TAGS, new JSONArray());
            jsonObject.put(Constants.JSON_DOCS, array);

        } while (false);
        return jsonObject;
    }

    private JSONArray getRelatedSearches(List<ScoreDoc> list, IndexSearcher indexSearcher)
            throws IOException, JSONException {

        JSONArray array = null;

        do {

            if (list == null || list.isEmpty())
                break;

            if (indexSearcher == null)
                break;

            Map<String, Map<String, Integer>> nerSet = new HashMap<String, Map<String, Integer>>();

            nerSet.put("per", new HashMap<String, Integer>());
            nerSet.put("loc", new HashMap<String, Integer>());
            nerSet.put("org", new HashMap<String, Integer>());
            nerSet.put("misc", new HashMap<String, Integer>());

            for (int i = 0; i < list.size(); ++i) {
                int docId = list.get(i).doc;
                Document doc = indexSearcher.doc(docId);
                TagCloud.getInstance().addToNerSet(doc, nerSet);
            }

            List<Tag> l = getNerList(nerSet);

            array = new JSONArray();
            for (Tag t : l) {
                JSONObject object = new JSONObject();
                object.put("text", t.getTag());
                object.put("weight", t.getOccurences());
                array.put(object);
            }
        } while (false);
        return array;
    }


    public JSONObject getSearchSuggestion(String searchString) {

        JSONObject jsonObject = null;
        do {
            try {

                if (searchString == null || searchString.isEmpty())
                    break;

                List<String> list = Arrays.asList(searchString.split("\\s"));

                DidYouMeanParserImpl didYouMeanParser =
                        new DidYouMeanParserImpl(Fields.FIELD_INDEX, FSDirectory.open(Paths.get(IndexServlet.cfgPaths.getSpellDir())));

                if (list.size() == 1) {
                    Query didyoumean = didYouMeanParser.suggest(searchString);
                    if (didyoumean != null) {
                        String suggestString = didyoumean.toString(Fields.FIELD_INDEX);
                        jsonObject = new JSONObject();
                        jsonObject.put(Constants.JSON_DIDYOUMEAN, suggestString);
                    }
                } else {
                    String suggestString = "";
                    for(String token : list) {
                        Query didyoumean = didYouMeanParser.suggest(token);
                        if (didyoumean == null) {
                            suggestString += token + " ";
                        } else {
                            suggestString += didyoumean.toString(Fields.FIELD_INDEX) + " ";
                        }
                    }

                    jsonObject = new JSONObject();
                    jsonObject.put(Constants.JSON_DIDYOUMEAN, suggestString);
                }
            } catch (IOException ioe) {
                LOGGER.error("io exception caught in getSearhSuggestion", ioe);
            } catch (JSONException jsone) {
                LOGGER.error("json exception caught in getSearhSuggestion", jsone);
            } catch (ParseException pe) {
                LOGGER.error("parse exception caught in getSearhSuggestion", pe);
            }
        } while (false);
        return jsonObject;
    }

    public TopDocs searchForOneResult(String searchString, IndexSearcher indexSearcher) {
        TopDocs topDocs = null;
        do {
            try {
                searchString = stemmer.stem(searchString);
                String words[] = searchString.split("\\s+");
                BooleanQuery bq = new BooleanQuery();
                for (int i = 0; i < words.length; i++) {
                    if (words[i].isEmpty())
                        continue;
                    TermQuery tq = new TermQuery(new Term(Fields.FIELD_URL_MD5, words[i]));
                    bq.add(tq, BooleanClause.Occur.MUST);
                }

                indexSearcher.setSimilarity(new BM25Similarity((float) 1.2, (float) 0.75));

                topDocs = indexSearcher.search(bq, 1);
                if (topDocs.totalHits == 0)
                    break;
            } catch (IOException ioe) {
                LOGGER.fatal("io excetion caught while in method search() ", ioe);
            }
        } while (false);
        return topDocs;
    }

    private Map<String, Map<String, Integer>> getSearchResultsInternal(ScoreDoc[] hits, JSONArray array, IndexSearcher indexSearcher,
                                                                       int low, int high) {

        Map<String, List<String>> videoMap = new HashMap<String, List<String>>();
        Map<String, Map<String, Integer>> nerSet = new HashMap<String, Map<String, Integer>>();

        nerSet.put("per", new HashMap<String, Integer>());
        nerSet.put("loc", new HashMap<String, Integer>());
        nerSet.put("org", new HashMap<String, Integer>());
   //     nerSet.put("misc", new HashMap<String, Integer>());

        try {
            for (int i = low; i < high; ++i) {
                int docId = hits[i].doc;
                Document doc = indexSearcher.doc(docId);
                if (isInSet(doc, videoMap))
                    continue;

                List<Series> series = inSeries(doc.get(Fields.FIELD_TITLE), indexSearcher);
                if (!series.isEmpty()) {
                    for (Series s : series) {
                        Document document = s.getDoc();
                        if (isInSet(document, videoMap))
                            continue;
                        addDocToJsonArray(document, array, true, videoMap);
                        TagCloud.getInstance().addToNerSet(document, nerSet);
                    }
                } else
                    addDocToJsonArray(doc, array, false, videoMap);

                TagCloud.getInstance().addToNerSet(doc, nerSet);
            }
        } catch (IOException e) {
            nerSet = null;
            LOGGER.error("io exception caught in getSearchResultsInternal() ", e);
        }
        return nerSet;
    }

    private JSONArray getSearchResultsFromScoreDocs(List<ScoreDoc> hits, IndexSearcher indexSearcher,
                                                    int low, int high) {

        JSONArray array = new JSONArray();
        Map<String, List<String>> videoMap = new HashMap<String, List<String>>();

        try {
            for (int i = low; i < high; ++i) {
                int docId = hits.get(i).doc;
                Document doc = indexSearcher.doc(docId);
                if (isInSet(doc, videoMap))
                    continue;

                List<Series> series = inSeries(doc.get(Fields.FIELD_TITLE), indexSearcher);
                if (!series.isEmpty()) {
                    for (Series s : series) {
                        Document document = s.getDoc();
                        if (isInSet(document, videoMap))
                            continue;
                        addDocToJsonArray(document, array, true, videoMap);
                    }
                } else {
                    addDocToJsonArray(doc, array, false, videoMap);
                }
            }
        } catch (IOException e) {
            LOGGER.error("io exception caught in getSearchResultsInternal() ", e);
        }
        return array;
    }

    public List<ScoreDoc> filterResults(ScoreDoc[] hits, IndexSearcher indexSearcher) {

        Map<String, List<String>> videoMap = new HashMap<String, List<String>>();
        List<ScoreDoc> list = new ArrayList<>();
        try {
            for (int i = 0; i < hits.length; ++i) {
                int docId = hits[i].doc;
                Document doc = indexSearcher.doc(docId);
                if (isInSet(doc, videoMap))
                    continue;

                List<Series> series = inSeries(doc.get(Fields.FIELD_TITLE), indexSearcher);
                if (!series.isEmpty()) {
                    for (Series s : series) {
                        Document document = s.getDoc();
                        if (isInSet(document, videoMap)) {
                            continue;
                        }

                        list.add(hits[i]);
                        addToVideoMap(document, videoMap);
                    }
                } else {
                    list.add(hits[i]);
                    addToVideoMap(doc, videoMap);
                }
            }
        } catch (IOException e) {
            LOGGER.error("io exception caught in getSearchResultsInternal() ", e);
        }
        return list;
    }

    public void addToVideoMap(Document d, Map<String, List<String>> videoMap) {

        String videoProvider = d.get(Fields.FIELD_VIDEO_PROVIDER);
        String videoId = d.get(Fields.FIELD_VIDEO_ID);

        boolean found = false;

        List<String> list = videoMap.get(videoProvider);
        if (list == null) {
            List<String> ll = new ArrayList<String>();
            ll.add(videoId);
            videoMap.put(videoProvider, ll);
        } else {
            for (String str : list) {
                if (str.equalsIgnoreCase(videoId)) {
                    found = true;
                }
            }

            if (!found) {
                list.add(videoId);
            }
        }
    }

    private List<Tag> getNerList(Map<String, Map<String, Integer>> nerSet) {

        List<Tag> l = TagCloud.getInstance().getNerList(nerSet.get("per"));
        l.addAll(TagCloud.getInstance().getNerList(nerSet.get("loc")));
        l.addAll(TagCloud.getInstance().getNerList(nerSet.get("org")));
        l.addAll(TagCloud.getInstance().getNerList(nerSet.get("misc")));


        Collections.shuffle(l);

        int OldMin = Integer.MAX_VALUE;
        int OldMax = Integer.MIN_VALUE;
        for (Tag t : l) {
            if (t.getOccurences() > OldMax)
                OldMax = t.getOccurences();
            else if (t.getOccurences() < OldMin)
                OldMin = t.getOccurences();
        }

        int min = 1;
        int max = 8;
        for (Tag t:l) {
            int X = ((t.getOccurences() - OldMin) * (max - min));
            int Y = (OldMax - OldMin) + min;
            int NewValue = X / Y;
            if (NewValue == 0)
                t.setOccurences(min);
            else
                t.setOccurences(NewValue);
        }

        return l;
    }

    private List<Series> inSeries(String title, IndexSearcher indexSearcher) {
        List<Series> list = new ArrayList<>();
        do {
            try {
                if (title == null || title.isEmpty())
                    break;

                Pattern urlPattern = Pattern.compile(Constants.REGEX_SERIES,
                        Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

                Matcher matcher = urlPattern.matcher(title);
                if (!matcher.find())
                    break;

                String subtitle = matcher.group("subtitle");
                String episode = matcher.group("episode");
                String number = matcher.group("number");

                System.out.println("subtitle: " + subtitle + " episode: " + episode + " number: " + number);

                subtitle = stemmer.stem(subtitle);

                TopDocs topDocs = search(subtitle, indexSearcher, 20, false);
                ScoreDoc[] hits = topDocs.scoreDocs;

                for (int i = 0; i < hits.length; i++) {
                    Document doc = indexSearcher.doc(hits[i].doc);
                    String title_series = doc.get(Fields.FIELD_TITLE);
                    matcher = urlPattern.matcher(title_series);
                    if (!matcher.find())
                        continue;

                    String subtitle_series = matcher.group("subtitle");
                    String episode_number = matcher.group("number");

                    subtitle_series = stemmer.stem(subtitle_series);

                    if (subtitle.equalsIgnoreCase(subtitle_series)) {
                        Series series = new Series(doc, Integer.parseInt(episode_number));
                        if (!list.contains(series)) {
                            list.add(series);
                        }
                    }
                }

                Collections.sort(list);

            } catch (IOException ioe) {
                LOGGER.error("io exception caught in inSeries() method ", ioe);
            }
        } while (false);
        return list;
    }

    public void addDocToJsonArray(Document d, JSONArray array, boolean inSeries, Map<String, List<String>> videoMap) {

        String videoProvider = d.get(Fields.FIELD_VIDEO_PROVIDER);
        String videoId = d.get(Fields.FIELD_VIDEO_ID);

        boolean found = false;

        List<String> list = videoMap.get(videoProvider);
        if (list == null) {
            List<String> ll = new ArrayList<>();
            ll.add(videoId);
            videoMap.put(videoProvider, ll);
        } else {
            for (String str: list) {
                if (str.equalsIgnoreCase(videoId)) {
                    found = true;
                }
            }

            if (!found) {
                list.add(videoId);
            }
        }

        JSONObject elem = new JSONObject();
        try {

            if (!inSeries)
                elem.put(Constants.JSON_SERIES, "0");
            else
                elem.put(Constants.JSON_SERIES, "1");

            elem.put(Constants.JSON_TITLE, d.get(Fields.FIELD_TITLE));
            elem.put(Constants.JSON_URL, d.get(Fields.FIELD_URL));
            elem.put(Constants.JSON_PARAGRAPH, d.get(Fields.FIELD_PARAGRAPH));
            elem.put(Constants.JSON_IMAGE, d.get(Fields.FIELD_IMAGE));
            elem.put(Constants.JSON_ID, d.get(Fields.FIELD_URL_MD5));

            Document document = SimilarityServlet.getDocumentTopic(d.get(Fields.FIELD_URL_MD5));
            if (document != null) {
                elem.put(Constants.JSON_TOPIC, document.get(Fields.LDA_FIELD_TOPIC));
            } else {
                elem.put(Constants.JSON_TOPIC, "0");
            }

            array.put(elem);
        } catch (JSONException j) {
            LOGGER.error("json exception caught while adding a json elem to the json array ", j);
        }
    }

    private boolean isInSet(Document document, Map<String, List<String>> videoMap) {

        String videoProvider = document.get(Fields.FIELD_VIDEO_PROVIDER);

        List<String> list = videoMap.get(videoProvider);
        if (list != null && !list.isEmpty()) {
            String videoId = document.get(Fields.FIELD_VIDEO_ID);
            for(String str : list) {
                if (videoId.equalsIgnoreCase(str)) {
                    return true;
                }
            }
        }
        return false;
    }


    public List<Doc> getSearchDocs(List<ScoreDoc> list, IndexSearcher indexSearcher, int low, int high)
            throws JSONException, IOException {

        List<Doc> result = new ArrayList<Doc>();
        JSONObject jsonObject = null;
        JSONArray array = new JSONArray();

        Map<String, List<String>> videoMap = new HashMap<>();

        do {
            if ((low > high) || (low < 0 || high < 0))
                break;

            for (int i = low; i < high; ++i) {
                int docId = list.get(i).doc;
                Document doc = indexSearcher.doc(docId);
                if (isInSet(doc, videoMap))
                    continue;

                List<Series> series = inSeries(doc.get(Fields.FIELD_TITLE), indexSearcher);
                if (!series.isEmpty()) {
                    for (Series s : series) {
                        Document document = s.getDoc();
                        if (isInSet(document, videoMap))
                            continue;
                        addToVMap(document, videoMap);
                        addToResultList(result, document, true);
                    }
                } else {
                    addToVMap(doc, videoMap);
                    addToResultList(result, doc, false);
                }
            }

            JSONArray tags = getRelatedSearches(list, indexSearcher);
            if (tags == null)
                break;

            jsonObject = new JSONObject();
            jsonObject.put(Constants.JSON_TAGS, tags);
            jsonObject.put(Constants.JSON_DOCS, array);

        } while (false);
        return result;
    }

    private void addToResultList(List<Doc> list, Document document, boolean inSeries) {
        Doc doc = new Doc(document.get(Fields.FIELD_URL_MD5),
                document.get(Fields.FIELD_URL),
                document.get(Fields.FIELD_TITLE),
                document.get(Fields.FIELD_IMAGE),
                document.get(Fields.FIELD_PARAGRAPH));

        if (!inSeries) {
            doc.setSeries("0");
        } else {
            doc.setSeries("1");
        }

        Document d = SimilarityServlet.getDocumentTopic(document.get(Fields.FIELD_URL_MD5));
        if (d != null) {
            doc.setTopic(document.get(Fields.LDA_FIELD_TOPIC));
        } else {
            doc.setTopic("-1");
        }

        list.add(doc);
    }

    private void addToVMap(Document d, Map<String, List<String>> videoMap) {

        String videoProvider = d.get(Fields.FIELD_VIDEO_PROVIDER);
        String videoId = d.get(Fields.FIELD_VIDEO_ID);

        boolean found = false;

        List<String> list = videoMap.get(videoProvider);
        if (list == null) {
            List<String> ll = new ArrayList<>();
            ll.add(videoId);
            videoMap.put(videoProvider, ll);
        } else {
            for (String str : list) {
                if (str.equalsIgnoreCase(videoId)) {
                    found = true;
                }
            }

            if (!found) {
                list.add(videoId);
            }
        }
    }
}
